#1. Write a program that prints a star pyramid.

class Pyramid
  def pyramidDrawMethod1
    star=0
    row=10
    col=10
   
    for i in 1..row do
      star=0
      for j in 1..row-i do
        print " "
      end 

      while star!=2*i-1
        print "*"
        star+=1    
      end
      print "\n"
    end
  end

  def pyramidDrawMethod2
    w=20; 1.upto(w) { |i| puts ">"+" "*(w-i)+"*"*(i*2-1)+" "*(w-i)+"<" }
  end
end

object=Pyramid.new() 
object.pyramidDrawMethod1 
object.pyramidDrawMethod2

