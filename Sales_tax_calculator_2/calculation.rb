class Calculation 
 def display()
   
   print "Enter Total Item \n"
   $totItem = gets.to_i

   if Integer($totItem)<=0
     print $errorMessage
   end 

   while $totItem >= 1
     $importStatus = ""   
     print "\nItem ",$count   

     print $itemTypeMessage
     itemType=gets.to_i 
     
     if itemType<=0 
       print $typeErrorMessage 
       next 
     end 
     
     print $checkImported
     checkImported=gets.to_i

     if checkImported<=0
       print $typeErrorMessage 
       next 
     end 

     print $itemName
     itemName=gets.chomp

     print $itemPrice
     itemPrice=gets.to_f
     
     if itemPrice<=0
       print $typeErrorMessage 
       next 
     end 

     print $itemQty
     itemQty=gets.to_i

     if itemQty<=0
       print $qtyError 
       next 
     end 
     
     if itemType == 1
       # No sale tax apply here  
       if checkImported == 1
         # import tax applay here
         importCalulate = (itemPrice * $importTaxAmount).round(2)
         $totalSaleTax = $totalSaleTax+importCalulate
         $importStatus = "Imported"
         $itemPriceFinal = (itemQty*(itemPrice+importCalulate))

       elsif checkImported == 2
         # No import and sale tax applay here
         $itemPriceFinal = (itemQty*itemPrice).round(2)

       else
         print $typeCheckErrorMessage
         
       end

     elsif itemType == 2
       # sale tax applay here 
       if checkImported == 1
         # import tax applay here
         $saleTaxCalulate = (itemPrice*($importTaxAmount + $saleTaxAmount)).round(2)
         $totalSaleTax = $totalSaleTax + $saleTaxCalulate
         $importStatus = "Imported"
         $itemPriceFinal = (itemQty * (itemPrice + $saleTaxCalulate))
        
       elsif checkImported == 2
         $saleTaxCalulate = (itemPrice * $saleTaxAmount).round(2)
         $totalSaleTax = $saleTaxCalulate + $totalSaleTax
         $itemPriceFinal = (itemQty * (itemPrice + $saleTaxCalulate)).round(2)
       end
     end

     $itemStore[$count] = "#{itemQty} #{$importStatus} #{itemName} : #{$itemPriceFinal} "     
     $totItem -= 1 
     $count += 1 
     
     $totalPrice = $itemPriceFinal + $totalPrice
    end
    
 end

end