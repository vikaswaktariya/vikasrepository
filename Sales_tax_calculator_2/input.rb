class Input
  def input() #use as a constructor
    $count=1 #global variable
    $totItem=0
    $itemStore=[]
    $importStatus=""
    $itemPriceFinal=0.00
    $saleTaxCalulate=0.00
    $totalSaleTax=0.00
    $totalPrice=0.00

    $saleTaxAmount=0.10
    $importTaxAmount=0.05

    #Messages 
    $itemTypeMessage="\nIf item type are 'medical,foods and book enter 1' or 'not press 2':"
    $checkImported="If item is 'imported enter 1' or 'not imported enter 2':" 
    $itemName="Enter Item Name:"
    $itemPrice="Enter Item Price:"
    $itemQty="Enter item quantity:"
    
    $errorMessage="Error: Enter a valid positive number" 
    $typeErrorMessage="Error: pleae enter only 1 or 2 value for this item"
    $qtyError="Error: Quantity must be greater than 0" 
    $priceError="Error: Price must be numeric value"
  end
end